import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthData } from 'src/app/auth/auth-data';
import { AuthSession } from 'src/app/auth/auth-session';
import { Countries } from "src/app/jsonfiles/countries"
import { UserSignupComponent } from './components/auth/user-signup/user-signup.component';
import { UserSigninComponent } from './components/auth/user-signin/user-signin.component';
import { AppSidebarComponent } from './components/layout/app-sidebar/app-sidebar.component';
import { UserdataIndexComponent } from './components/userdata/userdata-index/userdata-index.component';
import { AppHeaderComponent } from './components/layout/app-header/app-header.component';
import { AppFooterComponent } from './components/layout/app-footer/app-footer.component';
import { UserdataProfileComponent } from './components/userdata/userdata-profile/userdata-profile.component';
import { UserdataProfileEditComponent } from './components/userdata/userdata-profile-edit/userdata-profile-edit.component';
import { UserdataListComponent } from './components/userdata/userdata-list/userdata-list.component';
import { UserdataProfileCreateComponent } from './components/userdata/userdata-profile-create/userdata-profile-create.component';
import { UserdataProfileViewComponent } from './components/userdata/userdata-profile-view/userdata-profile-view.component';
import { UserdataDashboardComponent } from './components/userdata/userdata-dashboard/userdata-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    UserSignupComponent,
    UserSigninComponent,
    AppSidebarComponent,
    UserdataIndexComponent,
    AppHeaderComponent,
    AppFooterComponent,
    UserdataProfileComponent,
    UserdataProfileEditComponent,
    UserdataListComponent,
    UserdataProfileCreateComponent,
    UserdataProfileViewComponent,
    UserdataDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,
    NgbModule,
    NgSelectModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthSession, multi: true }, AuthData, Countries, NgbActiveModal],
  bootstrap: [AppComponent]
})
export class AppModule { }
