import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthData } from 'src/app/auth/auth-data';

@Injectable({
  providedIn: 'root'
})

export class AuthSession implements HttpInterceptor {
    constructor(private authData:AuthData) { }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(this.setAuthToken(httpRequest))
    }

    private setAuthToken(request: HttpRequest<any>) {
        const token = localStorage.getItem("TOKEN"); // current access Token
    
        return request.clone({
            setHeaders: {
              Authorization: `Bearer ${token}`
            }
        })
      }
  }