import { Auth } from '../models/auth.model';

export class AuthData {

    constructor() { }

    public isAdmin() : Boolean {
        let activeuser = this.getActiveUser();
        if (activeuser.roles?.includes("admin")) {
            return true
        }
        return false
    }

    public getActiveUser(): Auth {
        return JSON.parse(localStorage.getItem("activeuser") || "{}")
    }

    public getTOKEN(): String {
        // get current access Token
        return localStorage.getItem("TOKEN") || "";
    }
}
