import { Component, OnInit, Input } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  constructor() { }

  @Input() isAdmin: Boolean =false;
  @Input() myProfile: Auth = new Auth

  ngOnInit(): void {
  }

}
