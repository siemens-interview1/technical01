import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Auth } from 'src/app/models/auth.model';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Countries } from "src/app/jsonfiles/countries"


@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})

export class UserSignupComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private modalService: NgbModal, private countries: Countries) { }

  @ViewChild('modalData') modalData : any;
  showModal(modal: any): void {
    this.modalService.open(modal);
  }

  data: Auth = new Auth
  cpassword: String = ''
  signup_error: string = ''
  birthday: any
  countrylist: any
  countryname = ''

  ngOnInit(): void {
    this.countrylist = this.countries.list;
    this.prepare_custom_control()
  }
  
  prepare_custom_control(): void {
    let control: NodeListOf<Element> = document.querySelectorAll(".custom-control-input");

    control.forEach(function (item: any, index: number, arr: any) {
      item.checked ? item.parentNode.classList.add('checked') : null;
      item.addEventListener("change", function () {
        if (item.type === "radio") {
          document.querySelectorAll('input[name="' + item.name + '"]').forEach(function (item: any, index, arr: any) {
            item.parentNode.classList.remove('checked');
          });
          item.checked ? item.parentNode.classList.add('checked') : null;
        }
      });
    });
  }

  goSignin(): void {
    this.router.navigate(['/signin'])
  }

  select(d: any): void {
    debugger;
  }

  signup(): void {
    if (!this.birthday) return;

    this.data.birthdate= `${this.birthday.month}/${this.birthday.day}/${this.birthday.year}`;
    let data = this.data
    debugger;
    this.authService.signup(data)
      .subscribe({
        next: (res) => {
          data = new Auth;
          this.showModal(this.modalData)
        },
        error: (e) => {
          this.signup_error = e.error.message
          console.error(e.error);
        }
      });
  }
  

}
