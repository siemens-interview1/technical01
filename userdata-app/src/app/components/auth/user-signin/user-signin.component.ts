import { Component, OnInit } from '@angular/core';
import { UserdataService } from 'src/app/services/userdata.service';
import { AuthService } from 'src/app/services/auth.service';
import { AuthData } from 'src/app/auth/auth-data';
import { Auth } from 'src/app/models/auth.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-signin',
  templateUrl: './user-signin.component.html',
  styleUrls: ['./user-signin.component.css']
})

export class UserSigninComponent implements OnInit {

  auth: Auth = new Auth;
  loggedin: Boolean = false;
  signin_error: String = '';

  constructor(
    private authService: AuthService, 
    private userdataService: UserdataService, 
    private authData: AuthData,
    private router: Router) {  }

  ngOnInit(): void {
    //Clear session data from "localStorage"
    localStorage.setItem("TOKEN", '')
    localStorage.setItem("activeuser", '')
  }

  signin(): void {
    const data = {
      username: this.auth.username,
      password: this.auth.password
    };

    this.authService.signin(data)
      .subscribe({
        next: (res) => {
          this.signin_error = ''
          this.loggedin = true;
       
          //Store session data to "localStorage"
          localStorage.setItem("TOKEN", res.accessToken)
          localStorage.setItem("activeuser", JSON.stringify(res.userdata))

          //redirect here
          this.router.navigate(['/profile'])

        },
        error: (e) => {
          this.loggedin = false;
          this.signin_error = e.error.message;
          console.error(e.error.message);
        }
      });
  }


}
