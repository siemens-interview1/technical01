import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';
import { AuthData } from 'src/app/auth/auth-data';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Countries } from "src/app/jsonfiles/countries";

@Component({
  selector: 'app-userdata-profile',
  templateUrl: './userdata-profile.component.html',
  styleUrls: ['./userdata-profile.component.css']
})
export class UserdataProfileComponent implements OnInit {

  constructor(public countries: Countries, private modalService: NgbModal, public authData: AuthData) { }

  // input data from parent
  @Input() isAdmin: Boolean = false
  @Input() myProfile: Auth = new Auth

  profileData: Auth = new Auth
  birthDate: Date = new Date
  countryName: String = ''

  @ViewChild('modalData') modalData : any
  public closeModal(): void {
    this.triggerModal(this.modalData);
  }

  reloadProfile(data: Auth): void {
    //Store updated data to "localStorage"
    localStorage.setItem("activeuser", JSON.stringify(data))
  }

  ngOnInit(): void {
    if (this.myProfile != undefined) {
      // search country name by contry code
      let indx = this.countries.list.findIndex(item => item.code == this.myProfile?.country);
      this.countryName = this.countries.list[indx].name;
      
      //clone profile data
      this.profileData = Object.assign({}, this.myProfile);
      
      this.birthDate = new Date(String(this.profileData.birthdate));
    };
  }

  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((res) => {
      console.log(`Closed with: ${res}`);
    }, (res) => {
      console.log(`Closed with: ${res}`);
    });
  }

}
