import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataProfileComponent } from './userdata-profile.component';

describe('UserdataProfileComponent', () => {
  let component: UserdataProfileComponent;
  let fixture: ComponentFixture<UserdataProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
