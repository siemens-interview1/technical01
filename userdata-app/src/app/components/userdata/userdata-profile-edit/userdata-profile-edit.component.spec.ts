import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataProfileEditComponent } from './userdata-profile-edit.component';

describe('UserdataProfileEditComponent', () => {
  let component: UserdataProfileEditComponent;
  let fixture: ComponentFixture<UserdataProfileEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataProfileEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
