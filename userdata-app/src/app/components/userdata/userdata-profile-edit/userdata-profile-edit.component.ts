import {  } from '@angular/common';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';
import { NgbDate } from "@ng-bootstrap/ng-bootstrap";
import { UserdataService } from 'src/app/services/userdata.service';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-userdata-profile-edit',
  templateUrl: './userdata-profile-edit.component.html',
  styleUrls: ['./userdata-profile-edit.component.css']
})
export class UserdataProfileEditComponent implements OnInit {

  constructor(private userdataService: UserdataService, private modalService: NgbModal, private activeModal: NgbActiveModal) { }

   // input data from parent
  @Input() countryList?: Array<any>
  @Input() profileData: Auth = new Auth
  @Input() parentModal?: any
  @Input() reloadProfile?: any

  data: Auth = new Auth
  cpassword: String = ''
  update_error: string = ''
  birthday: any
  countrylist: any
  countryname = ''
  isadmin: boolean = false // edited user
  ndBirthDay: NgbDate  = new NgbDate(2000,1,1)

  @ViewChild('modalComplete') modalComplete : any;
  showModalComplete(): void {
    this.modalService.open(this.modalComplete);
  }

  ngOnInit(): void {
    if (this.profileData) {
      this.data = Object.assign({}, this.profileData);
      // check isAdmin?
      this.isadmin = ((this.data.roles?this.data.roles[0]:'') === 'admin')

      // convert date object for date control
      const dt = new Date(String(this.data.birthdate));
      this.ndBirthDay = new NgbDate(dt.getFullYear(),dt.getMonth()+1,dt.getDate())
      this.data.password = ""
    }
  }

  reloadRefProfile(data: Auth): void {
    // Refresh View Data
    this.profileData.birthdate = data.birthdate;
    this.profileData.country = data.country;
    this.profileData.email = data.email;
    this.profileData.empcode = data.empcode;
    this.profileData.fullname = data.fullname;
    this.profileData.gender = data.gender;
    this.profileData.lastlogin = data.lastlogin;
    this.profileData.mobile = data.mobile;
    this.profileData.roles = data.roles;
  }

  updateProfile(): void {
    let userId = this.data.username || '';
    // Convert NgbDate to String
    this.data.birthdate = `${this.ndBirthDay.month}/${this.ndBirthDay.day}/${this.ndBirthDay.year}`;

    // isadmin checked?
    if (this.isadmin) {
      this.data.roles = ['admin']
    } else {
      this.data.roles = ['user']
    }

    this.userdataService.patchById(userId, this.data)
    .subscribe({
      next: (res) => {
        this.update_error = '';
        //Update current profile data
        if (this.reloadProfile) this.reloadProfile(res);

        this.reloadRefProfile(res);

        //Update Modal
        setTimeout(() => this.parentModal.close(), 100)
        setTimeout(() => this.showModalComplete(), 300)
      },
      error: (e) => {
        this.update_error = e.error.message;
        console.error(e.error);
      }
    });
  }

}
