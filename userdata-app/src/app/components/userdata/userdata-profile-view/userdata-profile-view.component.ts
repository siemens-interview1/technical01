import { Component, OnInit, Input } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';

@Component({
  selector: 'app-userdata-profile-view',
  templateUrl: './userdata-profile-view.component.html',
  styleUrls: ['./userdata-profile-view.component.css']
})
export class UserdataProfileViewComponent implements OnInit {

  constructor() { }

 // input data from parent
  @Input() parentModal?: any
  @Input() profileData: Auth = new Auth
  @Input() countryList: Array<any> = new Array
  countryName: String = ''
  birthDate: Date = new Date

  ngOnInit(): void {
    if (this.profileData != undefined) {
      // search country name by contry code
      let indx = this.countryList.findIndex(item => item.code == this.profileData.country);
      this.countryName = this.countryList[indx].name;
      this.birthDate = new Date(String(this.profileData.birthdate));
    };
  }

}
