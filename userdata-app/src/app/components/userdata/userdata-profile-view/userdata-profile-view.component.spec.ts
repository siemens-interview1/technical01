import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataProfileViewComponent } from './userdata-profile-view.component';

describe('UserdataProfileViewComponent', () => {
  let component: UserdataProfileViewComponent;
  let fixture: ComponentFixture<UserdataProfileViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataProfileViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
