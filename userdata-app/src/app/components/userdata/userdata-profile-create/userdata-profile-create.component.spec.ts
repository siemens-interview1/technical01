import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataProfileCreateComponent } from './userdata-profile-create.component';

describe('UserdataProfileCreateComponent', () => {
  let component: UserdataProfileCreateComponent;
  let fixture: ComponentFixture<UserdataProfileCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataProfileCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataProfileCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
