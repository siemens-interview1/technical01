import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';
import { NgbDate } from "@ng-bootstrap/ng-bootstrap";
import { UserdataService } from 'src/app/services/userdata.service';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-userdata-profile-create',
  templateUrl: './userdata-profile-create.component.html',
  styleUrls: ['./userdata-profile-create.component.css']
})
export class UserdataProfileCreateComponent implements OnInit {

  constructor(private userdataService: UserdataService, private modalService: NgbModal) { }

   // input data from parent
  @Input() countryList?: Array<any>
  @Input() parentModal?: any
  @Input() reloadList?: any
  @Input() users: Array<Auth> = new Array

  data: Auth = new Auth
  cpassword: String = ''
  update_error: string = ''
  countrylist: any
  countryname = ''
  isadmin: boolean = false //new user role
  ndBirthDay: NgbDate  = new NgbDate(2000,1,1) //initial date for birthday


  @ViewChild('modalComplete') modalComplete : any;
  showModalComplete(): void {
    this.modalService.open(this.modalComplete);
  }

  ngOnInit(): void {
  }

  reloadUsers(res: any): void {
    this.users.unshift(res);
  }

  addData(): void {
    // check password and confirm password
    if (this.cpassword !== this.data.password) {
      this.update_error = 'Password not match!';
      return;
    }

    // isadmin checked?
    if (this.isadmin) {
      this.data.roles = ['admin']
    } else {
      this.data.roles = ['user']
    }

    // get date from date control then convert to string
    this.data.birthdate = `${this.ndBirthDay.month}/${this.ndBirthDay.day}/${this.ndBirthDay.year}`;
    this.userdataService.add(this.data)
    .subscribe({
      next: (res) => {
        this.update_error = '';

        //Update Modal
        setTimeout(() => this.parentModal.close(), 100)
        setTimeout(() => this.showModalComplete(), 300)
      },
      error: (e) => {
        this.update_error = e.error.message;
        console.error(e.error);
      }
    });
  }

}
