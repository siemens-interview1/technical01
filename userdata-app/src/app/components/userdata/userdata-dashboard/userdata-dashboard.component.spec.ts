import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataDashboardComponent } from './userdata-dashboard.component';

describe('UserdataDashboardComponent', () => {
  let component: UserdataDashboardComponent;
  let fixture: ComponentFixture<UserdataDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
