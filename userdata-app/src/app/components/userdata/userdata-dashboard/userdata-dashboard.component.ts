import { Component, OnInit, Input } from '@angular/core';
import colorLib from '@kurkle/color';
import { Chart, registerables } from 'chart.js';
import { Countries } from "src/app/jsonfiles/countries";
import { UserdataService } from 'src/app/services/userdata.service';
// declare jQuery
declare var $: any;
// declare var Chart: any

@Component({
  selector: 'app-userdata-dashboard',
  templateUrl: './userdata-dashboard.component.html',
  styleUrls: ['./userdata-dashboard.component.css']
})
export class UserdataDashboardComponent implements OnInit {

  constructor(private userDataService: UserdataService, private countries: Countries) { }

  @Input() isAdmin: Boolean = false;

  ngOnInit(): void {
    Chart.register(...registerables);
    this.initAgeChart();
    this.initGenderChart();
    this.initMapData();
  }

  initMapData(): void {
    this.userDataService.countbycountry()
    .subscribe({
      next: (res) => {
        this.createUsersMap(res);
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });
  }

  userMapData: Array<any> = new Array

  createUsersMap(data: any): void {
    // prepare map data (top 5) - join data with country list
    this.userMapData = data.slice(0, 5).map((obj: any) => {
      const code = obj.country;
      const res = this.countries.list.find((countryobj: any) => {
        return countryobj.code === code;
      });

      if (res) {
        return { name: res.name, count: obj.count }
      }
      return { name: "n/a", count: obj.count }
    });

    //prepare vector map data
    const mapdata: any = {};
    data.forEach((obj: any) => mapdata[obj.country.toLowerCase()] = obj.count);

    //init vector map function
    $('#worldMap').vectorMap({
      map: 'world_en',
      backgroundColor: 'transparent',
      borderColor: '#dee6ed',
      borderOpacity: 1,
      borderWidth: 1,
      color: '#ccd7e2',
      enableZoom: false,
      hoverColor: '#9cabff',
      hoverOpacity: null,
      normalizeFunction: 'linear',
      scaleColors: ['#ccd7e2', '#798bff'],
      selectedColor: '#6576ff',
      showTooltip: true,
      values: mapdata,
      onLabelShow: function (event: any, label: any, code: any) {
        let country = label.html()
        label.html(country + ': ' + (mapdata[code] || 0));
    }
    });
  }

  initGenderChart(): void {
    this.userDataService.countbygender()
    .subscribe({
      next: (res) => {
        this.createGenderData(res);
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });
  }

  totalUsers: number = 0
  genderDatas: Array<any> = new Array

  createGenderData(data: Array<any>): void {
    this.totalUsers = 0;
    data.forEach((gender) => {
      this.totalUsers += gender.count
    })

    this.genderDatas = data;
  }

  initAgeChart(): void {
    this.userDataService.countage()
    .subscribe({
      next: (res) => {
        this.createAgeChart(res);
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });
  }

   transparentize(value: any, opacity: any): any {
    var alpha = opacity === undefined ? 0.5 : 1 - opacity;
    return colorLib(value).alpha(alpha).rgbString();
  }

  createAgeChart(data: any): void {
    
    const ctx: any = document.getElementById('userAgeChart');
    if (!ctx) return;

    // Create Age Array [15 - 60]
    const chartlabes = Array.from([...Array(46).keys()], (g) => String(g + 15)) 

    // Prepare chart data
    const chartdatas = chartlabes.map((agelabel) => {
      const res = data.find((obj: any) => {
        return obj.age === agelabel;
      });
      return res ? res.count : 0 ;
    })

    // Chart theme colors 
    const CHART_COLORS = {
      red: 'rgb(255, 99, 132)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };

    // Find max of Y
    const vals = data.map((d:any) => d.count)
    const max_of_y = Math.max.apply(Math, vals);

    // Create chart by Chart.js = https://www.chartjs.org/docs/latest/
    const myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: chartlabes,
            datasets: [{
                label: 'count of Age',
                data: chartdatas,
                backgroundColor: this.transparentize(CHART_COLORS.blue, 0.5),
                borderColor: CHART_COLORS.blue,
                borderWidth: 1
            }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            legend: {
              display: true,
              labels: {
                boxWidth: 30,
                padding: 20,
              }
            }
          },
          scales: {
            x: {
              stacked: true,
              display: true,
              ticks: {
                
              },
              grid: {
                color: "transparent",
              },
              title: {
                display: true,
                text: 'Age'
              },
            },
            y: {
              stacked: true,
              ticks: {
                callback: function(val, index) {
                  // Hide every 2nd tick label
                  return index % 2 === 0 ? this.getLabelForValue(Number(val)) : '';
                },
              },
              max: max_of_y * 1.5,
              title: {
                display: true,
                text: 'Count'
              },
            }
          }
        }
    });
  }

}
