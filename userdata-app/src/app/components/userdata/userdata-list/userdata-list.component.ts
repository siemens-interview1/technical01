import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Countries } from "src/app/jsonfiles/countries";
import { UserdataService } from 'src/app/services/userdata.service';
import { DataexportService } from 'src/app/services/dataexport.service';

@Component({
  selector: 'app-userdata-list',
  templateUrl: './userdata-list.component.html',
  styleUrls: ['./userdata-list.component.css']
})
export class UserdataListComponent implements OnInit {

  constructor(
    public countries: Countries, 
    private modalService: NgbModal, 
    public userdataService: UserdataService, 
    private dataexportService: DataexportService, 
    public changeDetectorRef: ChangeDetectorRef
  ) { }

  @Input() isAdmin: Boolean = false;
  public users: Array<Auth> = new Array;
  selectedUser: Auth = new Auth;

  pageSize: number = 20;
  totalUsers: number = 0;
  totalPages: number = 0;
  currentPage: number = 1;
  searchParam: String = ''; 

  ngOnInit(): void {
    this.loadUserList();
  }

  onPageChange(page: any): void {
    this.currentPage = Number(page);
    this.loadUserList();
  }

  onPageSizeChange(pagesize: any): void {
    this.pageSize = Number(pagesize);
    this.currentPage = 1;
    this.loadUserList();
  }

  clearSearch(): void {
    this.searchParam = '';
    this.currentPage = 1
    this.loadUserList();
  }

  exportUserList(): void {
    
    this.dataexportService.exportuserlist()
    .subscribe({
      next: (res) => {
        // convert response to excel file and download
        const xlsxBlob = new Blob([res], { type: "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const downloadURL = window.URL.createObjectURL(xlsxBlob);
        const link = document.createElement('a');
        link.href = downloadURL;

        const fname = "user_list_export";
        link.download = `${fname}.xlsx`;
        link.click();
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });
  }

  searchUser(isNew: boolean): void {
    if (this.searchParam === '') return
    //first time search clear paging 
    let page = isNew ? 1 : this.currentPage;
    this.userdataService.search(this.searchParam, page, this.pageSize)
    .subscribe({
      next: (res) => {
        this.totalUsers = res.total;
        this.currentPage = res.page;
        this.totalPages = res.totalpage;
        this.users = res.users;
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });
  }

  loadUserList(): void {
    // check is searching by user
    if (this.searchParam !== '') {
      this.searchUser(false);
      return;
    }

    this.userdataService.page(this.currentPage, this.pageSize)
    .subscribe({
      next: (res) => {
        this.totalUsers = res.total;
        this.currentPage = res.page;
        this.totalPages = res.totalpage;
        this.users = res.users;
      },
      error: (e) => {
        debugger;
        console.error(e.error.message);
      }
    });
  }

  deleteUser(user: Auth): void {
    const username = user.username || '';
    if (username === '') return;
    this.userdataService.delete(username)
    .subscribe({
      next: (res) => {
        this.loadUserList();
      },
      error: (e) => {
        console.error(e.error.message);
      }
    });

  }

  triggerModal(content: any, selecteduser: Auth, modsize: string) {
    this.selectedUser = selecteduser;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: (modsize ? modsize : 'lg')}).result.then((res) => {
      console.log(`Closed with: ${res}`);
    }, (res) => {
      console.log(`Closed with: ${res}`);
    });
  }

}
