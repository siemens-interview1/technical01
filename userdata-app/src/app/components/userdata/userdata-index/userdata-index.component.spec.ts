import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataIndexComponent } from './userdata-index.component';

describe('UserdataIndexComponent', () => {
  let component: UserdataIndexComponent;
  let fixture: ComponentFixture<UserdataIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdataIndexComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdataIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
