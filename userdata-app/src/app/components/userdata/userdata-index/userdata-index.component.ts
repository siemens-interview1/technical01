import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/app/models/auth.model';
import { AuthData } from 'src/app/auth/auth-data';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-userdata-index',
  templateUrl: './userdata-index.component.html',
  styleUrls: ['./userdata-index.component.css']
})

//  =============== Use this page to render all User Data Pages ===============

export class UserdataIndexComponent implements OnInit {

  constructor(private authData: AuthData, router: Router) {

    //Check API session, if nothing redirect to Signin page
    if (authData.getTOKEN() === "" || !authData.getTOKEN()) {
      router.navigate(['/signin']);
    }

    //Switch content by route data
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) this.setContent(e.url); 
    });

  }

  isAdmin: Boolean = false;
  myProfile: Auth = new Auth;

  ngOnInit(): void {
    this.isAdmin = this.authData.isAdmin();
    this.myProfile = this.authData.getActiveUser();
  }

  // default / first route 
  content: String = '/profile'

  setContent(url: string): void {
    //switch content by route 
    this.content = url;
  }

}
