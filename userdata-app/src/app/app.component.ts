import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'siemens-hrms-interview';
  layoutstyle = '';

  constructor(router:Router, route: ActivatedRoute) {
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.setLayout(e.url);
      }
    });
  }

  setLayout(url: string): void {
    var index = ['/signin', '/signup'].indexOf(url);
    this.layoutstyle = index >=0 ? 'none' : 'full';
  }

  
}
