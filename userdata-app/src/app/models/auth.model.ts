export class Auth {
    username?: String;
    empcode?: String;
    gender?: String;
    fullname?: String;
    birthdate?: String;
    password?: String;
    email?: String;
    country?: String;
    mobile?: String;
    roles?: Array<String>;
    created?: Date;
    lastlogin?: Date;
  }
