import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserSignupComponent } from './components/auth/user-signup/user-signup.component';
import { UserSigninComponent } from './components/auth/user-signin/user-signin.component';
import { UserdataIndexComponent } from './components/userdata/userdata-index/userdata-index.component';

const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  { path: 'signin', component: UserSigninComponent },
  { path: 'signup', component: UserSignupComponent },
  { path: 'profile', component: UserdataIndexComponent },
  { path: 'user/list', component: UserdataIndexComponent },
  { path: 'user/dashboard', component: UserdataIndexComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
