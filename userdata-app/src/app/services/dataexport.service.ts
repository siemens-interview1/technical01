import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const serviceUrl = 'http://localhost:8080/api/export';

@Injectable({
  providedIn: 'root'
})
export class DataexportService {

  constructor(private http: HttpClient) { }

  // export user list to excel
  exportuserlist() {
    return this.http.get(`${serviceUrl}/userlist`, { responseType: 'blob' });
  }
}
