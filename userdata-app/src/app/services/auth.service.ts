import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const serviceUrl = 'http://localhost:8080/api/auth';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private http: HttpClient) { }

  signup(data: any): Observable<any> {
    return this.http.post(`${serviceUrl}/signup`, data);
  }

  signin(data: any): Observable<any> {
    return this.http.post(`${serviceUrl}/signin`, data);
  }

  signout(data: any): Observable<any> {
    return this.http.post(`${serviceUrl}/signout`, data);
  }

}
