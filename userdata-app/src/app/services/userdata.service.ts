import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Auth } from 'src/app/models/auth.model';
import { AuthSession } from 'src/app/auth/auth-session';

const serviceUrl = 'http://localhost:8080/api/userdata';

@Injectable({
  providedIn: 'root'
})

export class UserdataService {

  constructor(private http: HttpClient, private session: AuthSession ) { }

  getuser(userId: String): Observable<any> {
    return this.http.get(`${serviceUrl}/user/${userId}`);
  }

  patchById(userId: String, data: any): Observable<any> {
    return this.http.patch(`${serviceUrl}/patchById/${userId}`, data);
  }

  list(): Observable<any> {
    return this.http.get(`${serviceUrl}/list`);
  }

  page(page: number, pagesize: number): Observable<any> {
    return this.http.get(`${serviceUrl}/page/${page}/${pagesize}`);
  }

  search(param: String, page: number, pagesize: number): Observable<any> {
    return this.http.get(`${serviceUrl}/search/${param}/${page}/${pagesize}`);
  }

  add(data: any): Observable<any> {
    return this.http.post(`${serviceUrl}/add`, data);
  }

  delete(userId: String): Observable<any> {
    return this.http.delete(`${serviceUrl}/delete/${userId}`);
  }

  countage(): Observable<any> {
    return this.http.get(`${serviceUrl}/report/countage`);
  }

  countbygender(): Observable<any> {
    return this.http.get(`${serviceUrl}/report/countbygender`);
  }

  countbycountry(): Observable<any> {
    return this.http.get(`${serviceUrl}/report/countbycountry`);
  }

}

