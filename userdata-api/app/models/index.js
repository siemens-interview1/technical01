const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");

db._ROLES = [{name: "user", level: 0}, {name: "admin", level: 99}];
db._TITLES = [{title:"MR.", gender: "Male"}, {title:"MRS.", gender: "Female"}, {title:"MS.", gender: "Female"}];

module.exports = db;