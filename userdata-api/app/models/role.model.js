const mongoose = require("mongoose");

const _schema = new mongoose.Schema({
  name: String,
  level: Number
})

const Role = mongoose.model("Role_Data", _schema);

module.exports = Role;
