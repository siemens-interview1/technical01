const mongoose = require("mongoose");

const _schema = new mongoose.Schema({
  username: String,
  empcode: String,
  gender: String,
  fullname: String,
  birthdate: Date,
  email: String,
  password: String,
  country: String,
  mobile: String,
  roles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role_Data"
    }
  ],
  created: Date,
  lastlogin: Date,
})

const User = mongoose.model("User_Data", _schema);

module.exports = User;
