const { authenticationCls,authorizationCls,validateUserSignUp } = require("../security");
const controller = require("../controllers/userdata.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/userdata/user/:userId", [authenticationCls.verifyToken], controller.getuser);

  app.get("/api/userdata/list", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.list);

  app.get("/api/userdata/page/:pagenumber/:pagesize", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.page);

  app.get("/api/userdata/search/:searchparam/:pagenumber/:pagesize", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.search);

  app.patch("/api/userdata/patchById/:userId", [authenticationCls.verifyToken,validateUserSignUp.hasValidFieldsUpdate,], controller.patchById);

  app.post("/api/userdata/add",
    [
      validateUserSignUp.hasValidFields,
      validateUserSignUp.checkUser,
      validateUserSignUp.checkRole,
      authenticationCls.verifyToken,
      authorizationCls.isAdmin,
    ],
    controller.add
  );

  app.delete("/api/userdata/delete/:userId", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.deleteuser);

  app.get("/api/userdata/report/countage", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.dataAgeCount);

  app.get("/api/userdata/report/countbygender", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.dataGroupGender);

  app.get("/api/userdata/report/countbycountry", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.dataGroupCountry);

  app.get("/api/userdata/export/userlist", [authenticationCls.verifyToken,authorizationCls.isAdmin,], controller.exportlist);
};
