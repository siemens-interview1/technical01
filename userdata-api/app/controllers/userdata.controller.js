const config = require("../config/security.config");
const { authenticationCls } = require("../security");
// const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const crypto = require('crypto');
const XLSX = require('xlsx');
const path = require("path");
const Os = require('os');

const db = require("../models");
const User = db.user;
const Role = db.role;


// internal response user data function
const responseUser = (user, res) => {
  try {
    // convert Roles to Array of String
    let _roles = user.roles.map((r) => String(r.name));

    //send to client
    res.status(200).send({
      username: user.username,
      empcode: user.empcode,
      gender: user.gender,
      fullname: user.fullname,
      birthdate: user.birthdate,
      email: user.email,
      password: '',
      country: user.country,
      mobile: user.mobile,
      roles: _roles,
      created: user.created,
      lastlogin: user.lastlogin,
    });
  } catch (err) {
    console.log(err)
    res.status(500).send({ message: err });
  }
}

exports.deleteuser = async (req, res) => {
  let userId = req.params.userId //username

  // root user protect
  if (userId === "_root") {
    res.status(403).send({ message: "Cannot dalete root admin" });
    return;
  }

  // remove user by username
  let user = await User.findOneAndRemove({ username: userId })
  if (!user) {
    res.status(404).send({ message: "User Not found." });
    return;
  }

  // call response function
  responseUser(user, res);
}

exports.getuser = async (req, res) => {
  let userId = req.params.userId //username
  //get data by username
  let user = await User.findOne({ username: userId }).populate("roles", "-__v");
  if (!user) {
    res.status(404).send({ message: "User Not found." });
    return;
  }

  // call response function
  responseUser(user, res);
};

exports.list = async (req, res) => {
  // get all users from database
  let users = await User.find({}).sort("fullname").populate("roles", "-__v")

  // prepare data for client
  let users_res = users.map((user) => {
    let roles = user.roles.map((role) => role.name)
    return {
      username: user.username,
      empcode: user.empcode,
      gender: user.gender,
      fullname: user.fullname,
      birthdate: new Date(user.birthdate),
      email: user.email,
      password: '',
      country: user.country,
      mobile: user.mobile,
      roles: roles,
      created: new Date(user.created),
      lastlogin: new Date(user.lastlogin),
    }
  });

  const result = {
    total: users_res.count,
    users: users_res,
  }
  // return to client
  res.status(200).send(result);
};

exports.search = async (req, res) => {
  let searchobj = {};

  // search by username, fullname, empcode, email
  try {
    const param = req.params.searchparam
    const regex = new RegExp(param, 'i') //'i' for case insensitive
    const searchparams = [
      {"username": regex },
      {"fullname": regex },
      {"empcode": regex },
      {"email": regex },
    ]
    searchobj = { $or: searchparams};
  } catch(e) {
  }

  const pnumber = Number(req.params.pagenumber); //page number
  const psize = Number(req.params.pagesize); //page size
  const skip = (pnumber - 1) * psize; // start record number

  // get search count
  const total = await User.countDocuments(searchobj);
  const totalpage = Math.ceil(total/psize);

  // get search data limit by paging
  let searchusers = await User.find(searchobj).sort("fullname").skip(skip).limit(psize).populate("roles", "-__v")

  let users_res = searchusers.map((user) => {
    let roles = user.roles.map((role) => role.name)
    return {
      username: user.username,
      empcode: user.empcode,
      gender: user.gender,
      fullname: user.fullname,
      birthdate: new Date(user.birthdate),
      email: user.email,
      password: '',
      country: user.country,
      mobile: user.mobile,
      roles: roles,
      created: new Date(user.created),
      lastlogin: new Date(user.lastlogin),
    }
  });

  const result = {
    total: total,
    page: pnumber,
    totalpage: totalpage,
    users: users_res,
  }
  
  // return to client
  res.status(200).send(result);
};

exports.page = async (req, res) => {
  
  const pnumber = Number(req.params.pagenumber); //page number
  const psize = Number(req.params.pagesize); //page size
  const skip = (pnumber - 1) * psize;
  const total = await User.countDocuments({}); // get total records
  const totalpage = Math.ceil(total/psize);
  
  // get user data limit by paging
  let users = await User.find({}).sort("fullname").skip(skip).limit(psize).populate("roles", "-__v")

  let users_res = users.map((user) => {
    let roles = user.roles.map((role) => role.name)
    return {
      username: user.username,
      empcode: user.empcode,
      gender: user.gender,
      fullname: user.fullname,
      birthdate: new Date(user.birthdate),
      email: user.email,
      password: '',
      country: user.country,
      mobile: user.mobile,
      roles: roles,
      created: new Date(user.created),
      lastlogin: new Date(user.lastlogin),
    }
  });

  const result = {
    total: total,
    page: pnumber,
    totalpage: totalpage,
    users: users_res,
  }
  
  // return to client
  res.status(200).send(result);
};

exports.patchById = async (req, res) => {
  let userId = req.params.userId //username

  // if user update password
  if (req.body.password){
      req.body.password = bcrypt.hashSync(req.body.password, 8);
  } else {
    delete req.body.password;
  }

  //not update static data
  delete req.body.username
  delete req.body.created
  delete req.body.lastlogin
  delete req.body.__v
  delete req.body._id

  // get roles id from db
  let roles = await Role.find({name: { $in: req.body.roles }})
  req.body.roles = roles.map((role) => role._id);

  //update data
  let lastdata = await User.findOneAndUpdate({ username: userId }, req.body)

  //get new data
  let user = await User.findOne({ username: userId }).populate("roles", "-__v")

  // call response function
  responseUser(user, res);
};

exports.add = (req, res) => {
  // create User Object from request
  const user = new User({
    username: req.body.username,
    empcode: req.body.empcode,
    fullname: req.body.fullname,
    gender: req.body.gender,
    birthdate: req.body.birthdate,
    email: req.body.email,
    country: req.body.country,
    mobile: req.body.mobile,
    created: new Date(),
    lastlogin: 0,
    password: bcrypt.hashSync(req.body.password, 8),
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    // mapping with Database Roles
    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map((role) => role._id);
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            responseUser(user, res);
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          responseUser(user, res);
        });
      });
    }
  });
};

exports.dataAgeCount = async (req, res) => {
  // calculate user age and group by 
  let data = await User.aggregate([
    { 
      $project: {
          age: { 
            $floor: {
              $let:{
                  vars:{ diff: { $subtract: [ new Date(), "$birthdate"] } },
                  in: { $divide: ["$$diff", (365*24*60*60*1000)] }
              }
            }
          } 
      } 
    },
    {
      $group: { _id: "$age", count: { $sum: 1 } }
    },
    {
      $project: { _id: 0, "age": {$toString:"$_id"}, "count": 1 }
    }
  ])

  res.status(200).send(data)

}

exports.dataGroupGender = async (req, res) => {
  // calculate user gender and group by, count, min, max, avg
  let data = await User.aggregate([
    { 
      $project: {
          gender: 1,
          age: { 
            $floor: {
              $let:{
                  vars:{ diff: { $subtract: [ new Date(), "$birthdate"] } },
                  in: { $divide: ["$$diff", (365*24*60*60*1000)] }
              }
            }
          } 
      } 
    },
    {
      $group: { 
        _id: "$gender", 
        count: { $sum: 1 }, 
        avgage: { $avg: "$age" } ,
        minage: { $min: "$age" } ,
        maxage: { $max: "$age" } ,
      }
    },
    {
      $project: { _id: 0, "gender": "$_id", "count": 1, "avgage": 1, "minage": 1, "maxage": 1 }
    }
  ]).sort("gender")

  res.status(200).send(data)

}

exports.dataGroupCountry = async (req, res) => {
  // calculate user age and group by count
  let data = await User.aggregate([
    {
      $group: { 
        _id: "$country", 
        count: { $sum: 1 }, 
      }
    },
    {
      $project: { _id: 0, "country": "$_id", "count": 1 }
    }
  ]).sort("-count")

  res.status(200).send(data)

}

exports.exportlist = async (req, res) => {
  const wb = XLSX.utils.book_new(); //new workbook

  //prepare export data
  const users = await User.aggregate([
    {
      $lookup:
        {
          from: "role_datas",
          localField: "roles",
          foreignField: "_id",
          as: "user_role"
        }
    },
    { 
      $unwind: "$user_role" 
    },
    {
      $project: { 
        _id: 0, 
        "username": 1, 
        "role" : "$user_role.name",
        "empcode": 1, 
        "fullname": 1, 
        "gender": 1, 
        "email": 1, 
        "mobile": 1, 
        "country": 1,
        "birthdate": 1,
        "joined": "$created",
        "lastlogin": 1,
      }
    }
  ])

  // Convert to json format
  let temp = JSON.stringify(users);
  temp = JSON.parse(temp);

  const ws = XLSX.utils.json_to_sheet(temp);
  const filename = crypto.randomBytes(20).toString('hex');

  //check platform
  const isWin = (process.platform === "win32");
  // base dir + save location
  const dir = path.resolve(__dirname, "../../");
  let down = `${dir}/public/${filename}.xlsx`;
  if (isWin) {
    down = `${dir}\\public\\${filename}.xlsx`; 
  };

  // add worksheet
  XLSX.utils.book_append_sheet(wb, ws, "user_list");
  XLSX.writeFile(wb,down);

  res.download(down);
}