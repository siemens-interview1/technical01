const config = require("../config/security.config");
const { authenticationCls } = require("../security");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require("../models");
const User = db.user;
const Role = db.role;

exports.signup = (req, res) => {
  // create User Object from request
  const user = new User({
    username: req.body.username,
    empcode: req.body.empcode,
    fullname: req.body.fullname,
    gender: req.body.gender,
    birthdate: req.body.birthdate,
    email: req.body.email,
    country: req.body.country,
    mobile: req.body.mobile,
    created: new Date(),
    lastlogin: 0,
    password: bcrypt.hashSync(req.body.password, 8),
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    // mapping with Database Roles
    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map((role) => role._id);
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};

exports.signin = async (req, res) => {
  //find user by username from database
  User.findOne({
    username: req.body.username,
  })
  .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

      if (!passwordIsValid) {
        return res.status(401).send({ message: "Invalid Password!" });
      }

      // update last login
      User.findOneAndUpdate({ username: req.body.username }, { lastlogin: new Date() }).exec((err, _user) => {
        // convert Roles to Array of String
        var _roles = user.roles.map((r) => String(r.name));
        user.roles = _roles;
  
        // generate session token by user.id with 3 hours age
        let token = authenticationCls.generateToken(user.id, 3*60*60)
        req.session.token = token;
  
        // return to client
        res.status(200).send({
          userdata: {
            _id: user._id,
            username: user.username,
            empcode: user.empcode,
            gender: user.gender,
            fullname: user.fullname,
            birthdate: user.birthdate,
            email: user.email,
            password: '',
            country: user.country,
            mobile: user.mobile,
            roles: _roles,
            created: user.created,
            lastlogin: user.lastlogin,
          },
          accessToken: token,
        });
      });
    });
};