const data_validator = require('joi')
const db = require("../models");
const ROLES = db._ROLES;
const User = db.user;


hasValidFields = (req, res, next) => {
  // use Joi for data validation - https://www.npmjs.com/package/joi
  const schema = data_validator.object({
    username: data_validator.string().min(5).max(20).required(),
    email: data_validator.string().email().required(),
    password: data_validator.string().pattern(
      new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})") // basic password regex 
    ).required().messages({
      "string.pattern.base": "The password does not meet the password policy requirements."
    }),
    birthdate: data_validator.date().required(),
    fullname: data_validator.string().min(1).max(100).allow(),
    empcode: data_validator.string().min(1).max(50).allow(),
    country: data_validator.string().min(1).max(10).allow(),
    mobile: data_validator.allow(),
    gender: data_validator.string().min(1).max(10).required(),
    roles: data_validator.allow(),
  });

  const validate_result = schema.validate(req.body);
  if (validate_result.error) {
    res.status(400).send({ message: validate_result.error.details[0].message });
    return;
  }
  next();
}

hasValidFieldsUpdate = (req, res, next) => {
  // use Joi for data validation - https://www.npmjs.com/package/joi
  let schema = data_validator.object({
    _id: data_validator.allow(),
    username: data_validator.allow(),
    email: data_validator.string().email().required(),
    birthdate: data_validator.date().required(),
    fullname: data_validator.string().min(1).max(100).required(),
    empcode: data_validator.string().min(1).max(50).required(),
    country: data_validator.string().min(1).max(10).required(),
    mobile: data_validator.allow(),
    gender: data_validator.string().min(1).max(10).required(),
    roles: data_validator.allow(),
    created: data_validator.allow(),
    lastlogin: data_validator.allow(),
  });

  if (req.body.password !== '' || req.body.password){
    //if user update password
    let schemapassword = data_validator.object({
      password: data_validator.string().pattern(
        new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})") // basic password regex 
      ).required().messages({
        "string.pattern.base": "The password does not meet the password policy requirements."
      })
    })
    schema = schema.concat(schemapassword)
  } else {
    //if user not update password
    let schemapassword = data_validator.object({
      password: data_validator.allow(),
    })
    schema = schema.concat(schemapassword)
  }

  // validate input data
  const validate_result = schema.validate(req.body);
  if (validate_result.error) {
    res.status(400).send({ message: validate_result.error.details[0].message });
    return;
  }
  next();
}

checkUser = (req, res, next) => {
  // Check Username
  User.findOne({
    username: req.body.username
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(400).send({ message: "Failed! Username is already in use!" });
      return;
    }

    // Check Email
    User.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Failed! Email is already in use!" });
        return;
      }

      next();
    });
  });
};

checkRole = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.map((role) => role.name).includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Failed! Role ${req.body.roles[i]} does not exist!`
        });
        return;
      }
    }
  }
  next();
};

const verifySignUp = {
  hasValidFields,
  hasValidFieldsUpdate,
  checkUser,
  checkRole,
};

module.exports = verifySignUp;
