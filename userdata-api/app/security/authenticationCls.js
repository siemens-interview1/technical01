const jwt = require("jsonwebtoken");
const config = require("../config/security.config.js");
const db = require("../models");
const User = db.user;
const Role = db.role;

generateToken = (userid, expiresec) => {
  // generate session token by user.id with secretkey
  var token = jwt.sign({ id: userid }, config.secretkey, {
    expiresIn: expiresec, 
  });

  return token;
}

getToken = (req, res) => {

  // check access token
  if (!req.headers['authorization']) {
    return res.status(403).send({ message: "Token not found!" });
  }

  let authorization = req.headers['authorization'].split(' ');
  if (authorization[0] !== 'Bearer') {
    return res.status(403).send({ message: "Token not found!" });
  }

  let token = authorization[1];

  if (!token) {
    return res.status(403).send({ message: "Token not found!" });
  }

  return token;
}

verifyToken = (req, res, next) => {
  let token = getToken(req, res)

  // use jsonwebtoken to verify stored session data 
  jwt.verify(token, config.secretkey, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Invalid token!" });
    }
    // get userId
    req.userId = decoded.id;
    next();
  });
};

const authenticationCls = {
  generateToken,
  getToken,
  verifyToken,
};
module.exports = authenticationCls;
