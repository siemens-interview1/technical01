const db = require("../models");
const User = db.user;
const Role = db.role;

// Check user role
isAdmin = (req, res, next) => {
    User.findById(req.userId).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
  
      Role.find(
        {
          _id: { $in: user.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
  
          for (let i = 0; i < roles.length; i++) {
            if (roles[i].level >= 99) {
              next();
              return;
            }
          }
  
          res.status(403).send({ message: "Not permitted!" });
          return;
        }
      );
    });
  };

  const authorizationCls = {
    isAdmin,
  };
  module.exports = authorizationCls;