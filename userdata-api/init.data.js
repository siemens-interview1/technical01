const bcrypt = require("bcryptjs");
const initialdata = (db) => {
  // collections
  const Role = db.role;
  const User = db.user;

  // clone Roles data for use
  let ROLES = [...db._ROLES];

  // check/initial role data
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      // create roles data
      createRole()
    } else {
      // check is root created?
      createRootUser();
    }
  });

  const createRole = () => {
    if (ROLES.length > 0 && ROLES) {
      const ROLE = ROLES.shift();
      new Role({
            name: ROLE.name,
            level: ROLE.level
          }).save(err => {
            if (err) {
              console.log("error", err);
            }
            console.log(`prepare: added role  ${ROLE.name}.`);
            createRole()
          });
    } else {
      // Create root user
      createRootUser()
    }
  };

  const createRootUser = () => {
    User.estimatedDocumentCount((err, count) => {
      if (!err && count === 0) {
        let role =  Role.findOne({ name: 'admin' }, (err, role) => {
          new User({
            username: '_root',
            empcode: '-',
            gender: 'Male',
            birthdate: new Date(2000, 1, 1),
            email: 'root@mail.com',
            country: 'TH',
            mobile: '',
            fullname: 'Root Admin',
            password: bcrypt.hashSync("1234", 8),
            created: new Date(),
            lastlogin: 0,
            roles: [role._id]
          }).save(err => {
            if (err) {
              console.log("error", err);
            }
            console.log(`prepare: added root admin.`);
          });
        })
      }
    });

  };

}

module.exports = {initialdata: initialdata};