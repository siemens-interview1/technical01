const express = require("express");
const cors = require("cors");
const app = express();


// ======== Connect to database =========
const db = require("./app/models");
const dbConfig = require("./app/config/db.config");
const db_conn_str = `mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`;

// initial data class
const initCls = require("./init.data");

db.mongoose.connect(db_conn_str, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("DB Connected");
    // start insert init data
    initCls.initialdata(db);
  })
  .catch(err => {
    console.error("DB Connection error", err);
    process.exit();
});


// ============ Service Config ===========
const cookieSession = require("cookie-session");
const domainConfig = require("./app/config/domain.config");
const securityConfig = require("./app/config/security.config");

// read corsOptions from config
app.use(cors(domainConfig.corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// set cookie options
let cookieOptions = {
    name: "user-session",
    secret: securityConfig.secretkey, 
    httpOnly: true,
};
app.use(cookieSession(cookieOptions));

// first route
app.get("/", (req, res) => {
  res.json({ message: "Online" });
});


// ============ Routes Config ===========
// all routes
require("./app/routes/auth.routes")(app);
require("./app/routes/userdata.routes")(app);
require("./app/routes/export.routes")(app);


// ============ Start Server ===========
// set port from config and listen for requests
const SERVER_PORT = domainConfig.SERVER_PORT;
app.listen(SERVER_PORT, () => {
  console.log(`Server is online on port ${SERVER_PORT}.`);
});

